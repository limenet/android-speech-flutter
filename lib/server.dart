import 'dart:async';
import 'dart:collection';
import 'dart:io';

import 'dart:typed_data';

import './models.dart';
import 'dart:convert';
import 'package:http_server/http_server.dart';
import 'package:flutter_tts/flutter_tts.dart';

class Server {
  Server({this.onError, this.onData});

  Uint8ListCallback onData;
  DynamicCallback onError;
  HttpServer server;
  int port = 8765;
  bool running = false;

  start() async {
    runZoned(() async {
      FlutterTts flutterTts = FlutterTts();
      await flutterTts.setSpeechRate(1.0);
      await flutterTts.setVolume(1.0);
      await flutterTts.setPitch(1.0);

      server = await HttpServer.bind(InternetAddress.anyIPv4, port);
      server.transform(new HttpBodyHandler()).listen((HttpRequestBody body) async {
        LinkedHashMap json = body.body;
        String text = json['text'];
        String locale = json['locale'];
        await flutterTts.setLanguage(locale);
        await flutterTts.speak(text);
        this.onData(Uint8List.fromList(body.body.toString().codeUnits));
        body.request.response.write("Polly");
        body.request.response.close();
      });
      this.running = true;
      this.onData(Uint8List.fromList(
          ('Server listening on port ' + port.toString()).codeUnits));
    }, onError: (e) {
      this.onError(e);
    });
  }

  stop() async {
    await this.server.close();
    this.server = null;
    this.running = false;
  }
}
